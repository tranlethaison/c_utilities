/*Logger
History:
    180302 - SonTLT - Created
*/

#include "logger.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>

#include "utilities.h"

char *log_file = NULL; //All log messages will be appended to this file

bool is_log_created = false; //Keeps track whether the log file is created or not

FILE *file = NULL; //Log stream


/*Open log file
*/
bool open_log_file() {
    if(!log_file)
        return false;

    if(dose_file_exists(log_file)) {
        file = fopen(log_file, "a");
		is_log_created = true;
        printf("\n[Warning: %s][%s]", WAR_LOG_FILE_EXISTS, log_file);
    }

    if(!is_log_created) {
		file = fopen(log_file, "w");
		is_log_created = true;
	}
    
	if(!file) {
        printf("\n[Error: %s][%s][%s]", ERR_OPENING_LOG_FILE_FAILED, strerror(errno), log_file);

		if(is_log_created)
		    is_log_created = false;
		return false;
	}

    return true;
}


/*Begin log process
Call this before logging, otherwise no log will be written
*/
void begin_log(const char *log_file_name) {
    log_file = (char *) malloc(strlen(log_file_name) + 1);
    strncpy(log_file, log_file_name, strlen(log_file_name) + 1);
    open_log_file();
}


/*End log process, call this after done with logging
Closes the log stream
*/
void end_log() {
    log_file = NULL;
    is_log_created = false;
    if(file)
        fclose(file);
}


// Messages
const char ERR_OPENING_LOG_FILE_FAILED[] = "opening log file failed";
const char WAR_LOG_FILE_EXISTS[] = "log file exists. Appending";
