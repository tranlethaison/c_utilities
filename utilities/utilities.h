/* C utilities
History:
    180227 - SonTLT - Created
*/

#include <stdio.h>
#include <stdbool.h>

#ifndef UTILITIES_H
#define UTILITIES_H

/**/
bool dose_file_exists(const char *filename);


/**/
bool is_value_in_array(void *value, void **array, size_t size);


/**/
const char * get_current_date_time(const char *format);

#endif
